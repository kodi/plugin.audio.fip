#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import urllib
import urlparse

import xbmcaddon
import xbmcgui
import xbmcplugin


def get_streams():
    streams = {}
    base_stream_url = 'https://direct.fipradio.fr/live/'
    base_cover_url = 'https://www.fip.fr/sites/default/files/asset/images/'
    stream_suffix_parameter = '?ID=radiofrance'

    streams.update({
        0: {
            'title': u'fip',
            'url': base_stream_url + 'fip-midfi.mp3' + stream_suffix_parameter,
            'album_cover': 'https://www.fip.fr/sites/default/files/fip-quadri-filet.png'
        },
        1: {
            'title': u'fip autour du rock',
            'url': base_stream_url + 'fip-webradio1.mp3' + stream_suffix_parameter,
            'album_cover': base_cover_url + '2016/06/fip_autour_du_rock_logo_hd_player.png'
        },
        2: {
            'title': u'fip autour du jazz',
            'url': base_stream_url + 'fip-webradio2.mp3' + stream_suffix_parameter,
            'album_cover': base_cover_url + '2016/06/fip_autour_du_jazz_logo_hd_player.png'
        },
        3: {
            'title': u'fip autour du groove',
            'url': base_stream_url + 'fip-webradio3.mp3' + stream_suffix_parameter,
            'album_cover': base_cover_url + '2016/06/fip_autour_du_groove_logo_hd_player.png'
        },
        4: {
            'title': u'fip autour du monde',
            'url': base_stream_url + 'fip-webradio4.mp3' + stream_suffix_parameter,
            'album_cover': base_cover_url + '2016/06/fip_autour_du_monde_logo_hd_player.png'
        },
        5: {
            'title': u'tout nouveau tout fip',
            'url': base_stream_url + 'fip-webradio5.mp3' + stream_suffix_parameter,
            'album_cover': base_cover_url + '2016/06/fip_tout_nouveau_logo_hd_player.png'
        },
        6: {
            'title': u'fip autour du reggae',
            'url': base_stream_url + 'fip-webradio6.mp3' + stream_suffix_parameter,
            'album_cover': base_cover_url + '2017/01/fip-autour-du-reggae-hd-player.png'
        },
        8: {
            'title': u'fip autour de l\'electro',
            'url': base_stream_url + 'fip-webradio8.mp3' + stream_suffix_parameter,
            'album_cover': base_cover_url + '2017/05/fip-autour-de-l-electro-logo-hd-player.png'
        }
    })
    return streams


def build_url(query):
    base_url = sys.argv[0]
    return base_url + '?' + urllib.urlencode(query)


def build_streams_list(streams):
    stream_list = []
    for stream in streams:
        # create a list item using the stream filename for the label
        li = xbmcgui.ListItem(
            label=streams[stream]['title'], thumbnailImage=streams[stream]['album_cover'])
        # set the fanart to the albumc cover
        li.setProperty('fanart_image', streams[stream]['album_cover'])
        # set the list item to playable
        li.setProperty('IsPlayable', 'true')
        # build the plugin url for Kodi
        # Example: plugin://plugin.audio.example/?url=http%3A%2F%2Fwww.theaudiodb.com%2Ftestfiles%2F01-pablo_perez-your_ad_here.mp3&mode=stream&title=01-pablo_perez-your_ad_here.mp3
        url = build_url({
            'mode': 'stream',
            'url': streams[stream]['url'],
            'title': streams[stream]['title']
        })
        # add the current list item to a list
        stream_list.append((url, li, False))
    # add list to Kodi per Martijn
    # http://forum.kodi.tv/showthread.php?tid=209948&pid=2094170#pid2094170
    xbmcplugin.addDirectoryItems(addon_handle, stream_list, len(stream_list))
    # set the content of the directory
    xbmcplugin.setContent(addon_handle, 'songs')
    xbmcplugin.endOfDirectory(addon_handle)


def play_stream(url):
    play_item = xbmcgui.ListItem(path=url)
    xbmcplugin.setResolvedUrl(addon_handle, True, listitem=play_item)


def main():
    args = urlparse.parse_qs(sys.argv[2][1:])
    mode = args.get('mode', None)

    # initial launch of add-on
    if mode is None:
        streams = get_streams()
        build_streams_list(streams)
    # a stream from the list has been selected
    elif mode[0] == 'stream':
        # pass the url of the stream to play_stream
        play_stream(args['url'][0])


if __name__ == '__main__':
    addon_handle = int(sys.argv[1])
    main()
